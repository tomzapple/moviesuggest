/*
 * Copyright 2011 University of Minnesota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.grouplens.lenskit.hello;

import MovieSuggest.Business.Movie;
import com.mysql.jdbc.Connection;
import org.grouplens.lenskit.ItemRecommender;
import org.grouplens.lenskit.ItemScorer;
import org.grouplens.lenskit.Recommender;
import org.grouplens.lenskit.RecommenderBuildException;
import org.grouplens.lenskit.baseline.BaselineScorer;
import org.grouplens.lenskit.baseline.ItemMeanRatingItemScorer;
import org.grouplens.lenskit.baseline.UserMeanBaseline;
import org.grouplens.lenskit.baseline.UserMeanItemScorer;
import org.grouplens.lenskit.core.LenskitConfiguration;
import org.grouplens.lenskit.core.LenskitRecommender;
import org.grouplens.lenskit.data.sql.BasicSQLStatementFactory;

import org.grouplens.lenskit.knn.item.ItemItemScorer;
import org.grouplens.lenskit.scored.ScoredId;
import org.grouplens.lenskit.transform.normalize.BaselineSubtractingUserVectorNormalizer;
import org.grouplens.lenskit.transform.normalize.UserVectorNormalizer;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import org.grouplens.lenskit.data.sql.JDBCRatingDAO;
import org.grouplens.lenskit.knn.user.UserUserItemScorer;

/**
 * Demonstration app for LensKit. This application builds an item-item CF model
 * from a CSV file, then generates recommendations for a user.
 *
 * Usage: java org.grouplens.lenskit.hello.HelloLenskit ratings.csv user
 */
public class HelloLenskit implements Runnable {

    public static void main(String[] args) {
        HelloLenskit hello = new HelloLenskit();
        try {
            hello.run();
        } catch (RuntimeException e) {
            System.err.println(e.toString());
            e.printStackTrace(System.err);
            System.exit(1);
        }
    }

    private List<Integer> users;
    public static Map<Long, List<ScoredId>> recommendedUserRatings;

    public HelloLenskit() {

    }

//    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
//    static final String DB_URL = "jdbc:mysql://127.5.186.2:3306/moviesuggest";
static final String DB_URL = "jdbc:mysql://localhost:3306/moviesuggest";
    //  Database credentials
//    static final String USER = "admingM3YkPm";
//    static final String PASS = "ZHxkfCgEgX_5";

    static final String USER = "root";
    static final String PASS = "sesame";


    @Override
    public void run() {
        // We first need to configure the data access.
        // We will use a simple delimited file; you can use something else like
        // a database (see JDBCRatingDAO).
        recommendedUserRatings = new TreeMap<Long, List<ScoredId>>();

        Connection cxn = null;
        JDBCRatingDAO jdbcDAO = null;
        try {
            cxn = (Connection) DriverManager.getConnection(DB_URL, USER, PASS);
            jdbcDAO = new JDBCRatingDAO(cxn, new BasicSQLStatementFactory());

            /* additional configuration */
//    LenskitRecommender rec = LenskitRecommender.build( jdbcDAO);
    /* do things with the recommender */
        } catch (SQLException ex) {
            Logger.getLogger(HelloLenskit.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ;
        }

        LenskitConfiguration config = new LenskitConfiguration();
        config.addComponent(jdbcDAO);
        // Second step is to create the LensKit configuration...

        // ... and configure the item scorer.  The bind and set methods
        // are what you use to do that. Here, we want an item-item scorer.
        config.bind(ItemScorer.class)
                .to(ItemItemScorer.class);

        //config.bind(EventDAO.class).to(new SimpleFileRatingDAO(new File("ratings.csv"), ","));
        // let's use personalized mean rating as the baseline/fallback predictor.
        // 2-step process:
        // First, use the user mean rating as the baseline scorer
        config.bind(BaselineScorer.class, ItemScorer.class)
                .to(UserMeanItemScorer.class);
        // Second, use the item mean rating as the base for user means
        config.bind(UserMeanBaseline.class, ItemScorer.class)
                .to(ItemMeanRatingItemScorer.class);
        // and normalize ratings by baseline prior to computing similarities
        config.bind(UserVectorNormalizer.class)
                .to(BaselineSubtractingUserVectorNormalizer.class);

        // There are more parameters, roles, and components that can be set. See the
        // JavaDoc for each recommender algorithm for more information.
        // Now that we have a factory, build a recommender from the configuration
        // and data source. This will compute the similarity matrix and return a recommender
        // that uses it.
        Recommender rec = null;
        try {
            rec = LenskitRecommender.build(config);
        } catch (RecommenderBuildException e) {
            throw new RuntimeException("recommender build failed", e);
        }

        // we want to recommend items
        ItemRecommender irec = rec.getItemRecommender();
        assert irec != null; // not null because we configured one
        // for users
        for (long user : users) {
            // get 10 recommendation for the user
            List<ScoredId> recs = irec.recommend(user, 10);
            System.out.format("Recommendations for %d:\n", user);
            for (ScoredId item : recs) {
                System.out.format("\t%d\t%.2f\n", item.getId(), item.getScore());
            }
            recommendedUserRatings.put(user, recs);
        }
        try {
            cxn.close();
        } catch (SQLException ex) {
            Logger.getLogger(HelloLenskit.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

//    public Map<Long, List<ScoredId>> getRecommendedUserRatings(){
//        return this.recommendedUserRatings;
//    }
    public static List<MovieSuggest.Business.Movie> getRecommendedUserRatings(long userId) throws SQLException, ClassNotFoundException {
        // We first need to configure the data access.
        // We will use a simple delimited file; you can use something else like
        // a database (see JDBCRatingDAO).
        recommendedUserRatings = new TreeMap<Long, List<ScoredId>>();
        Connection cxn = null;
        JDBCRatingDAO jdbcDAO = null;
        try {
            cxn = getConnection();
            jdbcDAO = new JDBCRatingDAO(cxn, new BasicSQLStatementFactory());

            /* additional configuration */
            /* do things with the recommender */
        } catch (SQLException ex) {
            Logger.getLogger(HelloLenskit.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }

        LenskitConfiguration config = new LenskitConfiguration();
        config.addComponent(jdbcDAO);
        // ... and configure the item scorer.  The bind and set methods
        // are what you use to do that. Here, we want an item-item scorer.
        config.bind(ItemScorer.class)
                .to(ItemItemScorer.class);

        // let's use personalized mean rating as the baseline/fallback predictor.
        // 2-step process:
        // First, use the user mean rating as the baseline scorer
        config.bind(BaselineScorer.class, ItemScorer.class)
                .to(UserMeanItemScorer.class);
        // Second, use the item mean rating as the base for user means
        config.bind(UserMeanBaseline.class, ItemScorer.class)
                .to(ItemMeanRatingItemScorer.class);
        // and normalize ratings by baseline prior to computing similarities
        config.bind(UserVectorNormalizer.class)
                .to(BaselineSubtractingUserVectorNormalizer.class);

        // There are more parameters, roles, and components that can be set. See the
        // JavaDoc for each recommender algorithm for more information.
        // Now that we have a factory, build a recommender from the configuration
        // and data source. This will compute the similarity matrix and return a recommender
        // that uses it.
        Recommender rec = null;
        try {
            rec = LenskitRecommender.build(config);
        } catch (RecommenderBuildException e) {
            throw new RuntimeException("recommender build failed", e);
        }

        // we want to recommend items
        ItemRecommender irec = rec.getItemRecommender();
        assert irec != null; // not null because we configured one

        List<Long> users = Arrays.asList(userId);
        List<MovieSuggest.Business.Movie> recommendedMovies = new ArrayList<Movie>();
        Map<Long, String> movieList = HelloLenskit.getMovieList();
        // for users
        for (long user : users) {
            // get 10 recommendation for the user
            List<ScoredId> recs = irec.recommend(user, 10);
            System.out.format("Recommendations for %d:\n", user);
            for (ScoredId item : recs) {
                Movie movie = new Movie();
                movie.setId(item.getId());
                movie.setRating(item.getScore());
                movie.setName(movieList.get(item.getId()));
                recommendedMovies.add(movie);
                System.out.format("\t%d\t%.2f\n", item.getId(), item.getScore());
            }
        }
        cxn.close();
        return recommendedMovies;
    }

    public static Map<String, String> parseFBMovieRatingStringList(String movieRatingListString) throws SQLException, ClassNotFoundException {
        TreeMap<String, String> movieRatingList = new TreeMap<String, String>();
        String insertTableSQL = "INSERT INTO moviesuggest.facebook"
                + "(userID, movieName, rating) VALUES"
                + "(?,?,?)"
                + " ON DUPLICATE KEY UPDATE userID=userID, movieName=movieName ";
        Connection cxn = getConnection();
        String[] lines = movieRatingListString.split("\n");
        for (String line : lines) {
            StringTokenizer t = new StringTokenizer(line, "|");
            String movieName = t.nextToken();
            String movieRating = t.nextToken();
            movieRatingList.put(movieName, movieRating);
            PreparedStatement preparedStatement = cxn.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1, 9999);
            preparedStatement.setString(2, movieName);
            preparedStatement.setDouble(3, Double.parseDouble(movieRating));
            preparedStatement.executeUpdate();
            preparedStatement.close();

        }
        cxn.close();

        return movieRatingList;

    }

    public static void addMovieRatingsForUser() throws SQLException, ClassNotFoundException {

        Connection cxn = getConnection();
        String moviesDBQueryTableSQL = " SELECT DISTINCT movieTable.movieID, movieTable.movieName FROM (SELECT moviesuggest.ratings.item AS movieID, moviesuggest.movies.movieName AS movieName"
                + " FROM moviesuggest.ratings, moviesuggest.movies"
                + " WHERE moviesuggest.ratings.item = moviesuggest.movies.movieID) AS movieTable";

        PreparedStatement pstmt1 = cxn.prepareStatement(moviesDBQueryTableSQL); // create a statement
        ResultSet moviesTable = pstmt1.executeQuery();

        String facebookMoviesDBQueryTableSQL = "SELECT * FROM moviesuggest.facebook";
        PreparedStatement pstmt2 = cxn.prepareStatement(facebookMoviesDBQueryTableSQL);
        ResultSet fbMoviesTable = pstmt2.executeQuery();

        while (fbMoviesTable.next()) {
            String fbMovieName = fbMoviesTable.getString(2);
            moviesTable.first();
            while (moviesTable.next()) {
                String movieName = moviesTable.getString(2);
                if (movieName.contains(fbMovieName)) {

                    String insertTableSQL = "INSERT INTO moviesuggest.ratings"
                            + "(user, item, rating, timestamp) VALUES"
                            + "(?,?,?,?)"
                            //                            + "WHERE NOT EXISTS( SELECT * FROM moviesuggest.ratings WHERE user=? AND item=?)";
                            + " ON DUPLICATE KEY UPDATE user=user, item=item ";
                    int seconds = (int) ((int)System.currentTimeMillis() / 1000l);

                    PreparedStatement pstmt3 = cxn.prepareStatement(insertTableSQL);
                    pstmt3.setInt(1, fbMoviesTable.getInt(1));
                    pstmt3.setInt(2, moviesTable.getInt(1));
                    pstmt3.setInt(3, fbMoviesTable.getInt(3));
                    pstmt3.setInt(4, seconds);
                    pstmt3.executeUpdate();
                    pstmt3.close();
                }
            }
        }

        pstmt1.close();
        pstmt2.close();
        moviesTable.close();
        fbMoviesTable.close();

    }

    public static Connection getConnection() throws SQLException, ClassNotFoundException {

        Class.forName("com.mysql.jdbc.Driver");

        Connection cxn = (Connection) DriverManager.getConnection(DB_URL, USER, PASS);
        return cxn;
    }

    public static Map<Long, String> getMovieList() throws SQLException, ClassNotFoundException {

        Map<Long, String> movieList = new TreeMap<Long, String>();
        Connection cxn = getConnection();
        String fetchMovieSQL = " SELECT * from moviesuggest.movies";

        PreparedStatement pstmt1 = cxn.prepareStatement(fetchMovieSQL); // create a statement
        ResultSet moviesTable = pstmt1.executeQuery();
        while (moviesTable.next()) {
            Long movieId = moviesTable.getLong(1);
            String movieName = moviesTable.getString(2);
            movieList.put(movieId, movieName);
        }
        pstmt1.close();
        moviesTable.close();
        cxn.close();
        return movieList;
    }

    public static void cleanUp() throws SQLException, ClassNotFoundException {
        Connection cxn = getConnection();
        String deleteFBSQL = " DELETE from moviesuggest.facebook where userID=9999";
        PreparedStatement pstmt1 = cxn.prepareStatement(deleteFBSQL); // create a statement
        pstmt1.executeUpdate();
        pstmt1.close();
        String deleteRatingsSQL = " DELETE from moviesuggest.ratings where user=9999";
        PreparedStatement pstmt2 = cxn.prepareStatement(deleteRatingsSQL); // create a statement
        pstmt2.executeUpdate();
        pstmt2.close();
        cxn.close();

    }

}
