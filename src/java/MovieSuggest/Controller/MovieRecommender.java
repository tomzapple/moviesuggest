/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MovieSuggest.Controller;

import MovieSuggest.Business.Movie;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.Executors;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.AsyncContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.grouplens.lenskit.hello.HelloLenskit;
import org.grouplens.lenskit.scored.ScoredId;

/**
 *
 * @author thomsonverghese
 */
//@WebServlet(asyncSupported = true)
public class MovieRecommender extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {

        String url = "index.jsp";
        String userFBMovieListString = request.getParameter("movieList");
        try {
            HelloLenskit.cleanUp();
        } catch (SQLException ex) {
            Logger.getLogger(MovieRecommender.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MovieRecommender.class.getName()).log(Level.SEVERE, null, ex);
        }

        Map<String, String> userFBMovieList = null;
        try {
            userFBMovieList = HelloLenskit.parseFBMovieRatingStringList(userFBMovieListString);
            request.setAttribute("facebookMovies", userFBMovieList);
            
        } catch (SQLException ex) {
            Logger.getLogger(MovieRecommender.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MovieRecommender.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if (userFBMovieListString != null) {
            try {
                HelloLenskit.addMovieRatingsForUser();
            } catch (SQLException ex) {
                Logger.getLogger(MovieRecommender.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(MovieRecommender.class.getName()).log(Level.SEVERE, null, ex);
            }
        }


        String action = (String) request.getParameter("action");
        if (action.equals("suggest")) {

            AsyncContext aCtx = request.startAsync(request, response);
aCtx.setTimeout(60000);
            ScheduledThreadPoolExecutor executor = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(5);
//            executor.setKeepAliveTime(25, TimeUnit.SECONDS);
            executor.execute(new AsyncWebService(aCtx));

        }

    }

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    public class AsyncWebService implements Runnable {

        AsyncContext ctx;

        public AsyncWebService(AsyncContext ctx) {
            this.ctx = ctx;
        }

        @Override
        public void run() {

            try {
                
                String userIDString = ctx.getRequest().getParameter("userID");
                long userID = Long.parseLong(userIDString, 10);
                
                List<Movie> recommendedRatings = HelloLenskit.getRecommendedUserRatings(userID);

               
                ctx.getRequest().setAttribute("recommendedMovies", recommendedRatings);
                ctx.dispatch("/suggest.jsp");
            } catch (SQLException ex) {
                Logger.getLogger(MovieRecommender.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(MovieRecommender.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
