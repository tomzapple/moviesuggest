-- The ratings.csv and movies.csv are located at /var/lib/openshift/553efae7fcf9332771000086/app-root/runtime/dependencies/jbossews/webapps/moviesuggest/db
-- when changing teh dataset, run this script
-- 1.cd jbossews/webapps/moviesuggest/db
-- 2.msql
-- 3.source movielens.sql
-- 4.source import.sql


LOAD DATA LOCAL INFILE 'ratings.csv' 
INTO TABLE moviesuggest.ratings FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n';

LOAD DATA LOCAL INFILE 'movies.csv' 
INTO TABLE moviesuggest.movies FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n'