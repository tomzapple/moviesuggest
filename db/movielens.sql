DROP SCHEMA IF EXISTS moviesuggest;
CREATE SCHEMA moviesuggest ;
USE moviesuggest;
CREATE TABLE ratings (
  user BIGINT NOT NULL,
  item BIGINT NOT NULL,
rating double NOT NULL,
timestamp BIGINT NOT NULL,
CONSTRAINT unique_index UNIQUE (user,item)
) ;
-- ALTER TABLE moviesuggest.ratings ADD UNIQUE unique_index (user, item);

CREATE TABLE movies (
  movieID BIGINT NOT NULL,
  movieName varchar(100) NOT NULL,
genre varchar(50) NOT NULL
);

CREATE TABLE facebook (
  userID BIGINT NOT NULL,
  movieName varchar(50) NOT NULL,
  rating BIGINT NOT NULL,
CONSTRAINT unique_index UNIQUE (userID,movieName)
);
