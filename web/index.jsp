<%-- 
    Document   : cover
    Created on : Apr 28, 2015, 1:57:34 PM
    Author     : thomsonverghese
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Movie Suggest</title>

        <!-- Bootstrap core CSS -->
        <link href="bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="cover.css" rel="stylesheet">

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="../assets/ie-emulation-modes-warning.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <script>
            // This is called with the results from from FB.getLoginStatus().
            function statusChangeCallback(response) {
                console.log('statusChangeCallback');
                // The response object is returned with a status field that lets the
                // app know the current login status of the person.
                // Full docs on the response object can be found in the documentation
                // for FB.getLoginStatus().
                if (response.status === 'connected') {
                    // Logged into your app and Facebook.
                    testAPI();
                } else if (response.status === 'not_authorized') {
                    // The person is logged into Facebook, but not your app.
                    document.getElementById('status').innerHTML = 'Please log ' +
                            'into this app.';
                } else {
                    // The person is not logged into Facebook, so we're not sure if
                    // they are logged into this app or not.
                    document.getElementById('message').innerHTML =
                            'Log in to Facebook and let Movie Suggest recommend movies for you!';
                }
            }

            // This function is called when someone finishes with the Login
            // Button.  See the onlogin handler attached to it in the sample
            // code below.
            function checkLoginState() {
                FB.getLoginStatus(function (response) {
                    statusChangeCallback(response);
                });
            }

            window.fbAsyncInit = function () {
                FB.init({
                    appId: '1101078166586334',
                    cookie: true, // enable cookies to allow the server to access 
                    // the session
                    xfbml: true, // parse social plugins on this page
                    version: 'v2.2' // use version 2.2
                });

                // Now that we've initialized the JavaScript SDK, we call 
                // FB.getLoginStatus().  This function gets the state of the
                // person visiting this page and can return one of three states to
                // the callback you provide.  They can be:
                //
                // 1. Logged into your app ('connected')
                // 2. Logged into Facebook, but not your app ('not_authorized')
                // 3. Not logged into Facebook and can't tell if they are logged into
                //    your app or not.
                //
                // These three cases are handled in the callback function.

                FB.getLoginStatus(function (response) {
                    statusChangeCallback(response);
                });

            };

            // Load the SDK asynchronously
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

            // Here we run a very simple test of the Graph API after login is
            // successful.  See statusChangeCallback() for when this call is made.
            function testAPI() {
                console.log('Welcome!  Fetching your information.... ');
                FB.api('/me', function (response) {
                    console.log('Successful login for: ' + response.name);
                    document.getElementById('message').innerHTML =
                            'Thanks for logging in, ' + response.name + '!';

                    console.log('Fetching movie info  ');
                    FB.api('/me/video.rates?fields=data&limit=25', function (response) {
                        var data = response.data;
                        var movieRatings = '';
                        for (var i in data)
                        {
                            if (data[i].data.hasOwnProperty('movie')) {
                                var title = data[i].data.movie.title;
                                var rating = data[i].data.rating.value;
                                movieRatings = movieRatings + title + ' |' + rating + '\n';
                                console.log('Title:' + title + ' Rating:' + rating);
                            }

                        }

                        document.getElementById("movieListId").value = movieRatings;
                    });
                });
            }
        </script>

        <div class="site-wrapper">

            <div class="site-wrapper-inner">

                <div class="cover-container">

                    <div class="masthead clearfix">
                        <div class="inner">
                            <!--                            <h3 class="masthead-brand">Movie Suggest</h3>-->
                            <div class="fb-login-button" data-max-rows="1" data-size="xlarge" data-show-faces="true" data-auto-logout-link="true" scope="public_profile,email,user_actions.video,user_likes" onlogin="checkLoginState();" align = "right"></div>
                        </div>
                    </div>

                    <div id="inner-cover">
                        <div class="inner cover">
                            <h1 class="cover-heading">Get movie recommendations</h1>
                            <p class="lead" id="message">Log in to Facebook and let Movie Suggest recommend movies for you!  </p>

                            <p class="lead">
                            <form action="MovieRecommender" method="post" >
                                <input type="hidden" name="action" value="suggest">
                                <input type="hidden" name="userID" value="9999">
                                <input type="hidden" name="movieList" id="movieListId" />
                                <input class="btn btn-lg btn-default" type="submit" value="Suggest Me Movies!">
                            </form>
                            </p>
                        </div>
                    </div>
                    <div id="movie-tables">
                        <table class="table table-condensed">
                            <tr><h2>Movies you rated on Facebook </h2></tr>
                            <tr>
                                <th>Movie Name</th>
                                <th>Rating</th>
                            </tr>
                            <c:forEach var="item" items="${facebookMovies}">
                                <tr>
                                    <td>${item.key}</td>
                                    <td>${item.value}</td>
                                </tr>
                            </c:forEach>
                        </table>

                        <table class="table table-condensed">
                            <tr><h2>Movies You should watch</h2></tr>
                            <tr>
                                <th>Movie ID</th>
                                <th>Movie Name</th>
                                <th>Rating</th>
                            </tr>
                            <c:forEach var="item" items="${recommendedMovies}">
                                <tr>

                                    <td>${item.id}</td>
                                    <td>${item.name}</td>
                                    <td>${item.rating}</td>

                                </tr>
                            </c:forEach>
                        </table>
                    </div>

                    <c:if test="${param.action =='suggest'}">
                        <script>
                            document.getElementById('inner-cover').style.visibility = 'hidden';
                            document.getElementById("movie-tables").style.visibility = "visible"
                        </script>
                    </c:if>

                    <c:if test="${param.action == null}">
                        <script>
                            document.getElementById('inner-cover').style.visibility = 'visible';
                            document.getElementById("movie-tables").style.visibility = "hidden"
                        </script>
                    </c:if>



                    <div class="mastfoot">
                        <div
                            class="fb-like"
                            data-share="true"
                            data-width="450"
                            data-show-faces="true">
                        </div>
                        <div class="inner">
                            <p>Developed by <a href="">Thomson</a></a>.</p>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="../assets/ie10-viewport-bug-workaround.js"></script>
    </body>
</html>


